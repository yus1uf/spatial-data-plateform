import { pool } from "../database/connection.js";
import { PolygonQuery } from "../utilities/constant/constant.query.js";
import {customResponse} from '../utilities/helper/custom.response.js'
import { Message, Codes } from "../utilities/constant/constant.message.js";

export const createPolygons = async (req,res) => {
        try {
          const { name, coordinates } = req.body;
          if (!coordinates) {
            const response = customResponse({
                code: Codes.FORBIDDEN,
                message: Message.FORBIDDEN,
              })
              return res.status(Codes.FORBIDDEN).send(response);
          }
          const insertQuery = PolygonQuery.CREATE
          const values = [name, `POLYGON((${coordinates.join(',')}))`];
          const result = await pool.query(insertQuery, values);
          const response = customResponse({
            code: Codes.CREATED,
            message: Message.CREATED,
            data:result.rows[0]
          })
          return res.status(Codes.CREATED).send(response);
        } catch (error) {
          console.error(error);
          const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
        }
    }



export const getPolygons = async (req,res) => {
    try {
        const query = PolygonQuery.RETRIVE;
        const result = await pool.query(query);
        const response = customResponse({
            code: Codes.SUCCESS,
            message: Message.RETRIVE,
            data:result.rows
          })
        return res.status(Codes.SUCCESS).send(response);
      } catch (error) {
        console.error(error);
        const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
      }
}

export const getPolygonsById = async(req, res) => {
    try {
        const { id } = req.params;
    
        const retriveQuery = PolygonQuery.RETRIVE_BY_ID
        const values = [id];
    
        const result = await pool.query(retriveQuery, values);
    
        if (result.rowCount === 0) {
            const response = customResponse({
                code: Codes.NOT_FOUND,
                message: Message.NOT_FOUND,
                data:result.rows
              })
            return res.status(Codes.NOT_FOUND).send(response);
        } else {
            const response = customResponse({
                code: Codes.SUCCESS,
                message: Message.RETRIVE,
                data:result.rows
              })
            return res.status(Codes.SUCCESS).send(response);
        }
      } catch (error) {
        console.error(error);
        const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
      }
}

export const updatePolygonsById = async (req,res) =>{
    try {
        const { id } = req.params;
        const { name, coordinates } = req.body;
        if (!coordinates) {
            const response = customResponse({
                code: Codes.FORBIDDEN,
                message: Message.FORBIDDEN,
              })
              return res.status(Codes.FORBIDDEN).send(response);
          }
        const updateQuery = PolygonQuery.UPDATE;
        const values = [name, `POLYGON((${coordinates.join(', ')}))`, id];
    
        const result = await pool.query(updateQuery, values);
    
        if (result.rowCount === 0) {
            const response = customResponse({
                code: Codes.NOT_FOUND,
                message: Message.NOT_FOUND,
                data:result.rows
              })
            return res.status(Codes.NOT_FOUND).send(response);
        } else {
            const response = customResponse({
                code: Codes.SUCCESS,
                message: Message.UPDATED,
                data: result.rows[0]
              })
            return res.status(Codes.SUCCESS).send(response);
        }
      } catch (error) {
        console.error(error);
        const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
      }
}

export const deletePolygonsById = async (req, res) =>{
    try {
        const { id } = req.params;
        const deleteQuery = PolygonQuery.DELETE;
        const result = await pool.query(deleteQuery, [id]);
    
        if (result.rowCount === 0) {
            const response = customResponse({
                code: Codes.NOT_FOUND,
                message: Message.NOT_FOUND,
                data:result.rows
              })
            return res.status(Codes.NOT_FOUND).send(response);
        } else {
            const response = customResponse({
                code: Codes.SUCCESS,
                message: Message.DELETED,
                data: result.rows[0]
              })
            return res.status(Codes.SUCCESS).send(response);
        }
      } catch (error) {
        console.error(error);
        const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
      }
}