import express from 'express'
import {createPoints, deletePointsById, getPoints, getPointsById, updatePointsById} from '../controller/conntroller.points.js'

export const pointsRouter = express.Router()


pointsRouter.post('/add', createPoints)
pointsRouter.get('/get-all', getPoints)
pointsRouter.get('/get/:id', getPointsById)
pointsRouter.put('/update/:id', updatePointsById)
pointsRouter.delete('/delete/:id', deletePointsById)