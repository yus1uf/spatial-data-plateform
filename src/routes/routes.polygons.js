import express from 'express'
import {createPolygons,getPolygons,getPolygonsById,updatePolygonsById,deletePolygonsById} from '../controller/controller.polygons.js'

export const polygonsRouter = express.Router()


polygonsRouter.post('/add', createPolygons)
polygonsRouter.get('/get-all',getPolygons)
polygonsRouter.get('/get/:id', getPolygonsById)
polygonsRouter.put('/update/:id', updatePolygonsById)
polygonsRouter.delete('/delete/:id', deletePolygonsById )