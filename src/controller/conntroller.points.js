import { pool } from "../database/connection.js";
import { PointsQuery } from "../utilities/constant/constant.query.js";
import {customResponse} from '../utilities/helper/custom.response.js'
import { Message, Codes } from "../utilities/constant/constant.message.js";

export const createPoints = async (req,res) => {
        try {
          const { name, latitude, longitude } = req.body;
          const insertQuery = PointsQuery.CREATE
          const values = [name, longitude, latitude];
      
          const result = await pool.query(insertQuery, values);
          const response = customResponse({
            code: Codes.CREATED,
            message: Message.CREATED,
            data:result.rows[0]
          })
          return res.status(Codes.CREATED).send(response);
        } catch (error) {
          console.error(error);
          const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
        }
    }



export const getPoints = async (req,res) => {
    try {
        const query = PointsQuery.RETRIVE;
        const result = await pool.query(query);
        const response = customResponse({
            code: Codes.SUCCESS,
            message: Message.RETRIVE,
            data:result.rows
          })
        return res.status(Codes.SUCCESS).send(response);
      } catch (error) {
        console.error(error);
        const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
      }
}

export const getPointsById = async(req, res) => {
    try {
        const { id } = req.params;
    
        const retriveQuery = PointsQuery.RETRIVE_BY_ID
        const values = [id];
    
        const result = await pool.query(retriveQuery, values);
    
        if (result.rowCount === 0) {
            const response = customResponse({
                code: Codes.NOT_FOUND,
                message: Message.NOT_FOUND,
                data:result.rows
              })
            return res.status(Codes.NOT_FOUND).send(response);
        } else {
            const response = customResponse({
                code: Codes.SUCCESS,
                message: Message.RETRIVE,
                data:result.rows
              })
            return res.status(Codes.SUCCESS).send(response);
        }
      } catch (error) {
        console.error(error);
        const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
      }
}

export const updatePointsById = async (req,res) =>{
    try {
        const { id } = req.params;
        const { name, latitude, longitude } = req.body;
    
        const updateQuery = PointsQuery.UPDATE;
        const values = [name, longitude, latitude, id];
    
        const result = await pool.query(updateQuery, values);
    
        if (result.rowCount === 0) {
            const response = customResponse({
                code: Codes.NOT_FOUND,
                message: Message.NOT_FOUND,
                data:result.rows
              })
            return res.status(Codes.NOT_FOUND).send(response);
        } else {
            const response = customResponse({
                code: Codes.SUCCESS,
                message: Message.UPDATED,
                data: result.rows[0]
              })
            return res.status(Codes.SUCCESS).send(response);
        }
      } catch (error) {
        console.error(error);
        const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
      }
}

export const deletePointsById = async (req, res) =>{
    try {
        const { id } = req.params;
        const deleteQuery = PointsQuery.DELETE;
        const result = await pool.query(deleteQuery, [id]);
    
        if (result.rowCount === 0) {
            const response = customResponse({
                code: Codes.NOT_FOUND,
                message: Message.NOT_FOUND,
                data:result.rows
              })
            return res.status(Codes.NOT_FOUND).send(response);
        } else {
            const response = customResponse({
                code: Codes.SUCCESS,
                message: Message.DELETED,
                data: result.rows[0]
              })
            return res.status(Codes.SUCCESS).send(response);
        }
      } catch (error) {
        console.error(error);
        const response = customResponse({
            code: Codes.SERVER_ERROR,
            message:error.message,
            err: error
          })
          return res.status(Codes.SERVER_ERROR).send(response);
      }
}