export const PointsQuery = {
    CREATE: 'INSERT INTO point_data (name, location) VALUES ($1, ST_SetSRID(ST_MakePoint($2, $3), 4326)) RETURNING id',
    RETRIVE: 'SELECT id, name, ST_X(location) as longitude, ST_Y(location) as latitude FROM point_data ORDER BY id ASC',
    RETRIVE_BY_ID: 'SELECT id, name, ST_X(location) as longitude, ST_Y(location) as latitude FROM point_data WHERE id=$1',
    UPDATE: 'UPDATE point_data SET name = $1, location = ST_SetSRID(ST_MakePoint($2, $3), 4326) WHERE id = $4',
    DELETE: 'DELETE FROM point_data WHERE id = $1'
}

export const PolygonQuery = {
    CREATE: 'INSERT INTO polygon_data (name, boundary) VALUES ($1, ST_SetSRID(ST_GeomFromText($2), 4326)) RETURNING id',
    RETRIVE: 'SELECT id, name, ST_AsText(boundary) as coordinates FROM polygon_data ORDER BY id ASC',
    RETRIVE_BY_ID: 'SELECT id, name, ST_AsText(boundary) as coordinates FROM polygon_data WHERE id=$1',
    UPDATE: 'UPDATE polygon_data SET name = $1, boundary = ST_SetSRID(ST_GeomFromText($2), 4326) WHERE id = $3',
    DELETE:'DELETE from polygon_data WHERE id=$1'
}