# spatial-data-plateform


## Refer Below for API Documentation
 - [Click Here](https://documenter.getpostman.com/view/17963881/2s9YRFUVBg) || https://documenter.getpostman.com/view/17963881/2s9YRFUVBg
## Deployed Server Link

### [Click](https://spatial-data-crud.onrender.com/api/v1) [https://spatial-data-crud.onrender.com/api/v1]
## Defined Routes
- ### For Points Operation
  - /points/add
  - /points/get-all
  - /points/get/:id
  - /points/update/:id
  - /points/delete/:id

- ### For Polygons Operation
  - /Polygons/add
  - /Polygons/get-all
  - /Polygons/get/:id
  - /Polygons/update/:id
  - /Polygons/delete/:id



## Getting started With locally

To make it easy for you, here's a list of recommended next steps.


## Clone Project

```
cd existing_repo
git clone https://gitlab.com/yus1uf/spatial-data-plateform.git
cd spatial-data-plateform
npm i
```