import express from 'express'
const app = express();
import router from './routes/index.js';
// const router = require('./routes/route')


const port = process.env.PORT || 8085


app.use(express.json());

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use('/api/v1', router);
// app.get('/get-test',getUsers )
app.listen(port, () => {
  console.log(`app listening on port ${port}`);
});
