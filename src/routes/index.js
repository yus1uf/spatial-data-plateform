import { Router } from "express";

import { pointsRouter } from "./routes.points.js";
import { polygonsRouter } from "./routes.polygons.js";

const router = Router()

router.use('/points', pointsRouter)
router.use('/polygons',polygonsRouter)

export default router