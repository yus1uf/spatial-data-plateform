export const Message = {
    CREATED: 'Data Created..!',
    RETRIVE: 'Data Retrived..!',
    UPDATED: 'Data Updated..!',
    DELETED: 'Data Deleted..! ',
    NOT_FOUND: 'Data Not Found..!',
    FORBIDDEN: 'Invalid Data..!'
}

export const Codes = {
CREATED: 201,
SUCCESS: 200,
SERVER_ERROR: 500,
NOT_FOUND: 404,
FORBIDDEN: 403
}